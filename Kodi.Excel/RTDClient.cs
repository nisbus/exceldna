﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Kodi.Excel
{    
    public class RTDClient : IDisposable
    {
        bool isReconnecting = false;
        List<string> errorCache = new List<string>();
        CancellationTokenSource cancellation = new CancellationTokenSource();
        string progID = "kodi.excel";
        public RTDClient ()
	    {
	    }

        public void Send(RTDEvent eventData)
        {
            string data = JsonConvert.SerializeObject(eventData);
            if (isReconnecting)
            {
                if (errorCache.IndexOf(data) == -1)
                    errorCache.Add(data);
            }
            else
            {
                try
                {
                    using (NamedPipeClientStream stream = new NamedPipeClientStream(".", progID, PipeAccessRights.Write, PipeOptions.None, System.Security.Principal.TokenImpersonationLevel.Anonymous, HandleInheritability.None))
                    {
                        stream.Connect(150);

                        using (StreamWriter sw = new StreamWriter(stream))
                        {
                            sw.AutoFlush = true;
                            sw.WriteLine(data);
                            stream.WaitForPipeDrain();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error sending data to Excel " + ex.ToString());
                    if (errorCache.IndexOf(data) == -1)
                        errorCache.Add(data);
                    isReconnecting = true;
                    Reconnect();
                }
            }
        }

        private void Reconnect()
        {
            new Timer((_) =>
            {

                try
                {
                    using (NamedPipeClientStream stream = new NamedPipeClientStream(".", progID, PipeAccessRights.Write, PipeOptions.None, System.Security.Principal.TokenImpersonationLevel.Anonymous, HandleInheritability.None))
                    {
                        stream.Connect(150);
                        using (StreamWriter sw = new StreamWriter(stream))
                        {
                            foreach (var e in errorCache)
                            {
                                sw.AutoFlush = true;
                                sw.WriteLine(e);
                                stream.WaitForPipeDrain();
                            }
                            errorCache.Clear();
                            isReconnecting = false;
                        }
                    }
                }
                catch(Exception ex)
                {
                    Console.WriteLine("Error reconnecting to Excel "+ ex.ToString());
                    Reconnect();
                }
            }, null, 5000, Timeout.Infinite);
        }

        public void Dispose()
        {
            cancellation.Cancel();
        }
    }
}
