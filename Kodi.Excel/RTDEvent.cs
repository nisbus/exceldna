﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodi.Excel
{
    public class RTDEvent
    {
        public string TopicIdentifier { get; private set; }
        public object Value { get; private set; }

        public RTDEvent(string topicIdentifier, object value)
        {
            TopicIdentifier = topicIdentifier;
            Value = value;
        }
    }
}
