﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ExcelDna.ComInterop;
using ExcelDna.Integration;
using ExcelDna.Integration.Rtd;
using Newtonsoft.Json;

namespace Kodi.Excel
{    
    public class RTDServerAddIn : IExcelAddIn
    {
        public void AutoOpen()
        {            
            //Log("AutoOpen");
            //ExcelIntegration.RegisterUnhandledExceptionHandler(ex =>
            //    {                    
            //        Log("!!! EXCEPTION: " + ex.ToString());
            //        return null;
            //    });
            try
            {
                //Log("Registering addin");
                string xllPath = (string)XlCall.Excel(XlCall.xlGetName);
                var xlApp = (Microsoft.Office.Interop.Excel.Application)ExcelDnaUtil.Application;
                var addins = xlApp.AddIns.Add(xllPath, false /*don't copy file*/).Installed = true;

                //Log("Addin registered "+addins);
            }
            catch{ }
            ComServer.DllRegisterServer();
        }

        public void AutoClose()
        {
            //Log("AutoClose");
        }

        private void Log(string message)
        {
            if (!string.IsNullOrEmpty("C:\\RTDServer.log"))
            {
                using (var writer = File.AppendText("C:\\RTDServer.log"))
                    writer.Write(message+"\n");
            }
        }

    }

    [ProgId("kodi.excel.rtd"),
     ComVisible(true)]  
    public class RTDServer : ExcelRtdServer
    {        
        CancellationTokenSource cancellation = new CancellationTokenSource();
        //string _logPath;
        Dictionary<string, Topic> _topics;        
        //public string LogPath
        //{
        //    get 
        //    { 
        //        if(!string.IsNullOrEmpty(_logPath))
        //            return _logPath; 
        //        else
        //        {
                    
        //            _logPath = "C:\\RTDServer.log";
                    
        //            return _logPath;
        //        }
        //    }
        //    set { _logPath = value; }
        //}

        int GetTopicId(Topic topic)
        {
            return (int)typeof(Topic)
                        .GetField("TopicId", BindingFlags.GetField | BindingFlags.Instance | BindingFlags.NonPublic)
                        .GetValue(topic);
        }

        //protected override int Heartbeat()
        //{
        //    //Log("Heartbeat");
        //    return base.Heartbeat();
        //}

        protected override bool ServerStart()
        {
            _topics = new Dictionary<string, Topic>();    
            //Log("RTD Server Start");
            int bufferSize = 4096;
            PipeSecurity security = new PipeSecurity();
            security.AddAccessRule(new PipeAccessRule(new SecurityIdentifier(WellKnownSidType.BuiltinUsersSid, null), PipeAccessRights.ReadWrite, System.Security.AccessControl.AccessControlType.Allow));
            Type type = this.GetType();
            object[] attributes = type.GetCustomAttributes(typeof(ProgIdAttribute), true);
            string progID = "kodi.excel";
            if(attributes.Length > 0)
                progID = (attributes[0] as ProgIdAttribute).Value;
            Task.Factory.StartNew(() =>
            {
                try
                {
                    while (!cancellation.Token.IsCancellationRequested)
                    {
                        string data = string.Empty;
                            
                        using (NamedPipeServerStream stream = new NamedPipeServerStream(progID, PipeDirection.InOut, 1, PipeTransmissionMode.Message, PipeOptions.None, bufferSize, bufferSize, security, HandleInheritability.None))
                        {
                            stream.WaitForConnection();

                            using (StreamReader reader = new StreamReader(stream))
                            {
                                data = reader.ReadToEnd();
                            }
                        }

                        if (string.IsNullOrWhiteSpace(data) == false)
                        {
                            var eventData = JsonConvert.DeserializeObject<RTDEvent>(data);
                            if (eventData != null)
                            {                               
                                if(_topics.ContainsKey(eventData.TopicIdentifier))
                                {
                                    _topics[eventData.TopicIdentifier].UpdateValue(eventData.Value);
                                }
                            } 
                        }
                    }
                }
                catch
                {
                    //Log("Error starting server "+ex.Message+" : "+ex.StackTrace);
                }
            }, cancellation.Token);

            return true;
        }

        //protected override void ServerTerminate()
        //{
        //    cancellation.Cancel();
        //    //Log("Terminated RTD server");
        //}

        //private void Log(string message)
        //{
        //    if (!string.IsNullOrEmpty(_logPath))
        //    {
        //        using (var writer = File.AppendText(_logPath))
        //            writer.Write(message+"\n");
        //    }                        
        //}

        protected override object ConnectData(Topic topic, System.Collections.Generic.IList<string> topicInfo, ref bool newValues)
        {            
            var info = string.Join(";", topicInfo);
            //Log(string.Format("ConnectData: {0} - {{{1}}}", GetTopicId(topic), info));
            _topics.Add(info, topic);
            return "Loading...";
        }

        protected override void DisconnectData(Topic topic)
        {
            var idx = _topics.Values.ToList().IndexOf(topic);
            if(idx != -1)
                _topics.Remove(_topics.Keys.ElementAt(idx));
            //Log(string.Format("DisconnectData: {0}", GetTopicId(topic)));
        }
    }
}
