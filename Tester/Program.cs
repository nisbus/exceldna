﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kodi.Excel;

namespace Tester
{
    class Program
    {
        static RTDClient client;
        static void Main(string[] args)
        {
            client = new Kodi.Excel.RTDClient();
            Console.WriteLine("Press any key to send a random value to Excel");
            Loop();
        }

        private static void Loop()
        {
            var l = Console.Read();
            Random r = new Random();
            var d = new RTDEvent("1", r.NextDouble());            
            client.Send(d);
            Loop();
        }
    }
}
